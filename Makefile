# GNU Makefile for updating and uploading all pure-aur packages

# Stuff that you still have to do manually: Actually updating the pkgbuilds
# and pushing them to the AUR, as well as keeping the pkgbuild repo itself up
# to date (in particular, don't forget to commit and push to the main pkgbuild
# repo after updates in the submodules!).

# Note that to actually push updates to the pkgbuild repo you need to have the
# appropriate credentials. Likewise for pushing updates to the AUR; this also
# requires that you have set up the host aur-pure in your ~/.ssh/config file
# (see the remarks at the end of this Makefile).

# Also note that not all pkgbuilds are actually submodules right now, because
# they are not in the AUR (yet). At present, this affects the following
# packages:

# - faust2-git: We currently have to maintain our own version which differs
#   from the "official" AUR package in that it still uses LLVM 3.5. This is
#   necessary because Pure, until we finally get it ported to the MCJIT, won't
#   work with any later LLVM version.

# - pure-complete-git: This is a split package which currently can't be
#   installed directly from the AUR because the AUR doesn't really support
#   split packages very well yet, and because of bootstrapping issues (you
#   already need to have some Pure components installed in order to build it).

# Both packages are on the excludes list, along with the stuff in deps and the
# other Faust packages. These won't be built unless you specifically ask for
# it (using the `packages` make variable).

# Build and Upload Targets:

# make repo: Builds *all* outdated packages and uploads them to the (local)
# $(repobase)/$(reponame) directory (which may be just a local directory which
# will be created if necessary; but usually it points to some package
# repository hosted online). NOTE: This probably will *not* do the right thing
# *unless* you've already uploaded stuff previously or ran the `make update`
# target below. If you want to just build and upload *specific* packages, you
# can do this with: make repo packages="whitespace-delimited package list ..."

# make build: Like `make repo`, but this only builds outdated packages using
# makepkg without uploading them. You can still run `make repo` afterwards to
# do the upload in a separate step.

# make update: This just creates the *.build and *.upload time stamp files
# pretending that the current package versions already have been built and
# uploaded, without actually doing anything. This is useful if you begin with
# a fresh checkout of the pkgbuild repo *before* you begin updating some
# packages.

# make status: This does a quick check to see which packages would need to be
# uploaded and/or built if `make repo` or `make build` were run.

# Clean Targets:

# make clean: Removes all the *.build and *.upload time stamp files and the
# corresponding binary package (*.tar.xz) files, to pretend that nothing has
# been built and uploaded yet. Use this with caution, as a subsequent `make
# repo` will then rebuild and reupload all packages! This can be used to clean
# generated stuff except the sources downloaded by makepkg. You can then run
# `make update` afterwards to get the repo in the "uploaded" state.

# make realclean: This runs 'git clean -ffdx' both in the main repo and in the
# submodules to clean everything, including any sources downloaded by makepkg,
# leaving the pkgbuild repo in pristine state. It goes without saying that
# this should be used with even more caution.

.PHONY: all repo repo-update build build-update update status clean realclean

# Packages to be excluded from the default packages list. These won't be built
# and uploaded unless specified explicitly.
excludes = deps/% pure-complete-git

# The package list.
packages ?= $(filter-out $(addsuffix /, $(addprefix ./, $(excludes))), $(sort $(dir $(shell find . -name PKGBUILD 2>/dev/null))))

# We reuse the same makefile to recursively build and upload each
# package listed in $(packages) by switching on MAKELEVEL.
ifeq (0,${MAKELEVEL})

# Comment this out for verbose output (entering and exiting directories).
rmakeopt = --no-print-directory

# Setup for recursive make invocations.
rmakeopt += -C $$package -f "$(CURDIR)/Makefile" AURMAKEFILE="$(CURDIR)/Makefile" arch=$(arch) repobase=$(repobase) reponame=$(reponame) makepkgflags="$(makepkgflags)"

# By default, we build packages for the host system architecture. NOTE: You
# shouldn't try to change this right now, as cross-architecture builds aren't
# supported by the Makefile yet.
arch := $(shell uname -m)

# Packages go to $(repodir)/$(arch) in the parent directory by default, this
# directory will be created if it doesn't exist yet. This is where I keep my
# clone of the pureaur.bitbucket.org repo on which the packages are hosted
# (https://bitbucket.org/pureaur/pureaur.bitbucket.org). The repository is
# named $(reponame), this provides the name for the package database
# (.db.tar.gz) files. If you're uploading packages elsewhere, or just want to
# keep them in a local directory then you'll want to adjust this accordingly.
reponame = pure-aur
repodir  = pureaur.bitbucket.org
repobase = $(CURDIR)/../$(repodir)/$(arch)

all:
		@echo "Run 'make repo' to build outdated packages and upload them,"
		@echo "or 'make build' to just build outdated packages."
		@echo "Package repository is $(reponame) at:"
		@echo "$(repobase)"
		@echo "It will be created if necessary."

repo: ; @for package in $(packages); do $(MAKE) repo $(rmakeopt); done

build: ; @for package in $(packages); do $(MAKE) build $(rmakeopt); done

update: ; @for package in $(packages); do $(MAKE) update $(rmakeopt); done

status: ; @for package in $(packages); do $(MAKE) status $(rmakeopt); done

else

# The expected target is worked out by sourcing the PKGBUILD file.
# This will not work quite right for -git packages, where the makepkg
# process also updates the version.
pkgname := $(shell source ./PKGBUILD && echo $$pkgname)
ifeq ($(shell source ./PKGBUILD && echo $$epoch),)
xprefix := $(shell source ./PKGBUILD && echo $$pkgname-$$pkgver-$$pkgrel)
prefix := $(xprefix)
else
# We have to go to some lengths here to get versions including an epoch prefix
# to expand properly in a rule. (make doesn't like the extra colon there
# unless it's escaped.)
xprefix := $(shell source ./PKGBUILD && echo $$pkgname-$$epoch'\:'$$pkgver-$$pkgrel)
prefix := $(shell source ./PKGBUILD && echo $$pkgname-$$epoch:$$pkgver-$$pkgrel)
endif
repofile := $(prefix).upload
buildfile := $(prefix).build
repofilex := $(xprefix).upload
buildfilex := $(xprefix).build

# Name of the binary package file. The PKGBUILD determines whether this is
# 'any' or the host architecture ('x86_64' or 'i686').
ifeq ($(shell source ./PKGBUILD && echo $${arch[0]}), any)
pkgfile := $(prefix)-any.pkg.tar.xz
pkgfilex := $(xprefix)-any.pkg.tar.xz
else
pkgfile := $(prefix)-$(arch).pkg.tar.xz
pkgfilex := $(xprefix)-$(arch).pkg.tar.xz
endif

all:
		@echo "No target given."

update: ;	touch $(buildfile) ; touch $(repofile)

build:		$(buildfilex)

repo:		$(repofilex)

# Extra targets to update the time stamps and perform an upload to the binary
# package repository. These are invoked recursively so that the version and
# revision numbers in $(prefix) are updated if they happen to be changed
# during the build. This may happen if the package is built from git or hg
# sources.

build-update:
		touch $(buildfile)

repo-update:
		test -d $(repobase) || mkdir -p $(repobase)
		@find $(repobase) -regextype posix-awk -regex '.*/$(pkgname)-r?[0-9][^/]*\.pkg\.tar\.xz' -exec echo rm -f '{}' ';' 2>/dev/null || true
		@find $(repobase) -regextype posix-awk -regex '.*/$(pkgname)-r?[0-9][^/]*\.pkg\.tar\.xz' -exec rm -f '{}' ';' 2>/dev/null || true
		cp $(pkgfile) $(repobase)
		cd $(repobase) && repo-add $(reponame).db.tar.gz $(pkgfile)
# Kludge alert: pacman apparently requires the $(reponame).db file, and
# repo-add creates this as a symbolic link to $(reponame).db.tar.gz. But that
# doesn't work if we keep the database in a git repo (at least not on
# Bitbucket), so we rather create a copy of the database file instead.
		cd $(repobase) && rm -f $(reponame).db && cp $(reponame).db.tar.gz $(reponame).db
# Kludge alert #2: Latest makepkg creates these. Get rid of them.
		cd $(repobase) && rm -f $(reponame).files* $(reponame)*.old
		touch $(repofile)

# At present, this isn't 100% accurate as it only checks for the existence of
# the time stamps. So this will fail to report packages if the PKGBUILD was
# changed without bumping the version/revision number. Oh well.
status:
		@test -f $(repofile) || echo "$(pkgfile) needs to be uploaded"
		@test -f $(buildfile) || echo "$(pkgfile) needs to be built"

endif

$(repofilex):	$(buildfilex)
		$(MAKE) -f "$(AURMAKEFILE)" repo-update arch=$(arch) repobase=$(repobase) reponame=$(reponame)

##############################################################################
# This is the guts of the Makefile were we actually invoke makepkg to build
# the packages.

# You can use either makepkgflags or makeflags to pass extra flags to makepkg.
# This should help speed things up a bit if you have multiple cores:
#makeflags=-j4
makepkgflags ?= $(if $(makeflags),MAKEFLAGS="$(makeflags)")

# This cleans up src and pkg directories after the build (-c) so that the next
# build always starts from a pristine state again. (However, note that src is
# *not* cleaned in advance. This is done on purpose so that `make build` and
# `make repo` can use existing state if you ran makepkg manually beforehand,
# which may be useful in some situations.) We also get rid of old versions of
# the package beforehand so that they do not clutter the package directory.
$(buildfilex):	PKGBUILD
		rm -f $(pkgname)-*.pkg.tar.xz;
		unset arch && makepkg -fc $(makepkgflags)
		$(MAKE) -f "$(AURMAKEFILE)" build-update

##############################################################################

# Clean binaries. This removes the binary packages along with the
# corresponding timestamps.
clean:
		@find . -regextype posix-awk -regex '.*\.(build|upload|pkg\.tar\.xz)' -exec echo rm -f '{}' ';' 2>/dev/null || true
		@find . -regextype posix-awk -regex '.*\.(build|upload|pkg\.tar\.xz)' -exec rm -f '{}' ';' 2>/dev/null || true

# This just cleans everything, going back to pristine state. Use with caution!!
realclean:
		git clean -ffdx
		git submodule foreach git clean -ffdx

##############################################################################

# Migrate a package to submodules.  Requires a aur-pure line in ~/.ssh/config.
# Something like:
#
#  Host aur-pure
#  HostName aur4.archlinux.org
#  User aur
#  IdentityFile ~/.ssh/id_rsa.aur4.pure
%.sm-migrate:
		git rm -rf $*
		rm -rf $*
		git submodule add --force ssh://aur-pure/$(notdir $(*D)) $*

sm-migrate:	$(foreach package,$(packages),$(package).sm-migrate)
