
# GNU Makefile: Use this to build your own collection of -git packages for the
# rolling Pure release.

# You'll need the usual packaging tools to make this work (makepkg, repo-add).
# Also make sure that the PKGBUILD and this Makefile are in their own subdir,
# since the build process will create a bunch of intermediate files.

# Then run `make repo repobase=./pure-aur-git` to build all packages and put
# the package repository in the given directory. (You can also run `make
# build` to just build the packages in the current directory instead, without
# creating a repository.)

# The target directory and the package repository will be created if they
# don't exist yet. Location and name of the repository can be adjusted in
# various ways with the repo* make variables, see below.

# BOOTSTRAPPING

# makepkg will try to install any missing dependencies when you run this for
# the first time. This also includes some Pure components which are not
# available in the standard Arch repositories. So, chances are that you
# haven't installed Pure yet, but you'll need some parts of it to build the
# Pure packages! To resolve this catch-22, you can either install the
# required Pure packages from the AUR or use our binary Pure repositories. For
# the latter, temporarily add the following section to your pacman.conf:

# [pure-aur-git]
# SigLevel = Never
# Server = https://pureaur.bitbucket.io/git/$arch

# Now run `sudo pacman -Sy` and you should be set. After running `make repo`,
# you can remove the pure-aur-git packages installed by makepkg again, and
# also remove the Bitbucket repository from pacman.conf (then run `sudo pacman
# -Sy` again). Finally install your freshly built Pure package collection and
# you're ready to go!

# If you subsequently rebuild the package collection, then you'll be using
# your existing Pure installation and there's no need to jump through all
# these hoops again. Of course, you might also just go with our pure-aur-git
# repository and be done with it. :)

SHELL=/bin/bash

arch := $(shell uname -m)

repodir = pureaur.bitbucket.org
reponame = pure-aur-git
repobase = $(realpath $(CURDIR)/../..)/$(repodir)/git/$(arch)

prefix := $(shell source ./PKGBUILD && echo $$pkgbase-$$pkgver-$$pkgrel)
repofile := $(prefix).upload
buildfile := $(prefix).build

# This should help speed things up a bit if you have multiple cores:
#makeflags=-j4
makepkgflags ?= $(if $(makeflags),MAKEFLAGS="$(makeflags)")

all:
	@echo "Run 'make repo' to build and upload package repo."
	@echo "Run 'make build' to only build packages."
	@echo "Current package revision: $(prefix)"
	@echo "Repository: $(repobase)/$(reponame).db"
	@echo "MAKEFLAGS can be passed to makepkg like this: 'make repo makeflags=-j4'"

repo: $(repofile)

build: $(buildfile)

update: ; touch $(buildfile); touch $(repofile)

$(buildfile): PKGBUILD
	rm -f *.pkg.tar.xz
	makepkg -s $(makepkgflags)
	touch $(buildfile)

$(repofile): $(buildfile)
	repo-add $(reponame).db.tar.gz *.pkg.tar.xz
	test -d $(repobase) || mkdir -p $(repobase)
	rm -f $(repobase)/$(reponame).db* $(repobase)/*.pkg.tar.xz
	mv $(reponame).db.tar.gz *.pkg.tar.xz $(repobase)
	cp $(repobase)/$(reponame).db.tar.gz $(repobase)/$(reponame).db
	rm -f $(reponame).db* $(reponame).files*
	touch $(repofile)

clean:
	@find . -regextype posix-awk -regex '.*\.(build|upload|pkg\.tar\.xz)' -exec echo rm -f '{}' ';' 2>/dev/null || true
	@find . -regextype posix-awk -regex '.*\.(build|upload|pkg.tar.xz)' -exec rm -f '{}' ';' 2>/dev/null || true

# NOTE: This will only work inside the git repo. If you downloaded PKGBUILD
# and Makefile as individual files then you'll have to use `make clean` above.
realclean:
	git clean -ffdx .
